package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private String nom;
	private List<Ingredient> ingredientList;
	private long id;
	
	 public PizzaDto() {}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Ingredient> getIngredientList() {
		return ingredientList;
	}

	public void setIngredientList(List<Ingredient> ingredientList) {
		this.ingredientList = ingredientList;
	}
	
	public boolean getIngredientExist(Ingredient ingredient) {
		return this.ingredientList.contains(ingredient);
	}
	
	public void setIngredient(Ingredient nouveau, Ingredient ancien) {
		for (Ingredient ing : ingredientList) {
			if (ing.equals(ancien)) {
				ing = nouveau;
			}
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	 
}
