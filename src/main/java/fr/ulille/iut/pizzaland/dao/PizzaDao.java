package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
public interface PizzaDao {
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (id INTEGER PRIMARY KEY, idIngredient FOREIGN KEY(Ingredient) references Ingredients(id))")
	void createAssociationTable();
	
	@SqlUpdate("Drop table if exists PizzaIngredientsAssociation")
	void dropTabel();
	
	@SqlUpdate("Insert into PizzaIngredientsAssociation(name, ingredient) values(:name, :ingredient)")
	@GetGeneratedKeys
	long insert(String name, Ingredient[] liste);
	
	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropTable();

	@SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);

	@SqlQuery("SELECT * FROM pizzas")
	//@SqlQuery("Select * from PizzaIngredientsAssociation")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();
	
	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	//@SqlQuery("SELECT * FROM pizzas as p, PizzaIngredientsAssociation as pi WHERE p.id = pi.id and pi.id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);

	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);

	default void createTableAndIngredientAssossiation() {
		createAssociationTable();
		createPizzaTable();
	}

	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void remove(long id);

}
