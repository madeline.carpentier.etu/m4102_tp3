package fr.ulille.iut.pizzaland.beans;


import java.util.ArrayList;
import java.util.List;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private List<Ingredient> pizzaIngredient;
	private String name;
	private long id;
	
	public Pizza() {
		this.pizzaIngredient = new ArrayList<Ingredient>();
	}
	
	public Pizza(long id, String name) {
		this.id = id;
		this.name = name;
		this.pizzaIngredient = new ArrayList<Ingredient>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String nom) {
		this.name = nom;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Ingredient> getPizzaIngredient() {
		return pizzaIngredient;
	}

	public void setPizzaIngredient(List<Ingredient> pizzaIngredient) {
		this.pizzaIngredient = pizzaIngredient;
	}
	
	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setNom(i.getName());

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getNom());

		return pizza;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (pizzaIngredient == null) {
			if (other.pizzaIngredient != null)
				return false;
		} else if (!pizzaIngredient.equals(other.pizzaIngredient))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pizza [pizzaIngredient=" + pizzaIngredient + "]";
	}
	
	public static PizzaCreateDto toCreateDto(Pizza pizza) {
	    PizzaCreateDto dto = new PizzaCreateDto();
	    dto.setName(pizza.getName());
	    
	    return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
	    Pizza pizza = new Pizza();
	    pizza.setName(dto.getName());

	    return pizza;
	}
}
